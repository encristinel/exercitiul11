public class TestGeneric {
    public static void main(String[] args) {

        Generic<String> angajat1 = new Generic<>("Samuel Jackson");
        Generic<String> angajat2 = new Generic<>("Plaza");
        Generic<Integer> salariu = new Generic<>(2000);

        System.out.println("My name is " +angajat1.getAngajat());
        System.out.println("I work as cashier at " +angajat2.getAngajat() +" hotel.");
        System.out.println("My salary is valued somewhere around " +salariu.getAngajat() +" dollars.");


    }
}